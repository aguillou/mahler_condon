# Experimentation around speed of convergence in Boyd-Lawton theorem for Mahler measure

The file `condon.sage` is a script Sage (9.2, but should not be very different from version to version), with and heavy use of MPsolve in the background. Numerical computations use ARB in Sage, to keep tracks of erros. Both sage and MPSolve have to be installed.

It is probable that this will only work under Linux (maybe also macos, to be tested)

This scrit allows to compute efficiently sequence of Mahler measures of polynomials P(x,x^d) where P is a 2-variate polynomial with integer coefficients.

If you know by some other means the Mahler measure of P (e.g. using https://github.com/liuhangsnnu/mahler-measure-of-genus-2-and-3-curves/), it will compute the difference m(P_d)-m(P).


### Usage

Basic usage is along the following, in sage:
```
load('condon.sage')

P_infty = Ring1(x*y*((x+1/x) + (y+1/y)+1))
mP_infty = 0 # Or the actual value if you know it

Erreur = erreur(d1,d2, P_inf = P_infty, mP_inf=mP_infty)
```
Erreur will contain a list of (d, erreur) where d is explained above and erreur is the estimated erreur, as a ball (err.mid, err.rad).

If you want to plot this data, use something along the lines of:
```
points([(d,err.mid()) for d, err in Erreur], marker='o', size=2)
```

An optional argument of erreur is `outfile`: if you add `outfile=your_file_location`, it will automatically export the liste Erreur in a txt file. Example:
```
Erreur = erreur(40,1000, P_inf = P_infty, mP_inf=mP_infty,outfile = 'test2.txt')
```
