#### script sage pour expérimenter autour des résultats de Condon

import shlex, subprocess

# La précision à laquelle les calculs sont tentés, et les strucure arb
# pour travailler en certifié
digits = 36
RBF = RealBallField(digits)
CBF = ComplexBallField(digits)

# Par défaut, c'est le polynome x*y*(3*(x+1/x) + 2*(y+1/y)+1)
# On a calculé sa mesure de Mahler par ailleurs (cf
	#https://github.com/liuhangsnnu/mahler-measure-of-genus-2-and-3-curves/
Ring1.<x,y> = PolynomialRing(ZZ,'x,y') 
P_infty = Ring1(x*y*(3*(x+1/x) + 2*(y+1/y)+1))
mP_infty = RBF(.40546510810816438197801311546434913657)


# Les fonctions. La principale est la première: erreur(d1,d2) produit la 
#liste des erreurs m(P_infty(x,x^d))-m(P_infty) pour d entre d1 et d2


def erreur(d1,d2, P_inf = P_infty, mP_inf =mP_infty, digits = digits, 
		outfile=False, plot_error=True):
	Erreur = []
	for d in range(d1,d2):
		Erreur.append((d,mP_d(d,P_inf, digits=digits)-mP_inf))
	if type(outfile)==str:
		save_error(Erreur, d1,d2,P_inf,mP_inf,outfile)
	return(Erreur)
	
def save_error(Erreur, d1,d2 ,P_inf,mP_inf,outfile):
    Intro="""Liste m(P(x,x^d))-m(P) pour
P = %s
m(P) = %s
d entre %i et %i

"""%(P_inf,mP_inf,d1,d2)
    with open(outfile,'w') as temp:
        temp.write(Intro)
        for i, err in Erreur:
            temp.write('d=%i: %s\n'%(i,err))

def mP_d(d,P_inf, digits=digits):
    roo = roots(d,P_inf)
    return sum([r.abs().log() for r in roo])

def roots(d,P_inf,digits=digits):
    """Take a polynomial P_inf in ZZ[x,y]
    
    returns the roots of modulus >1 of P = P_inf(x,x^d), as 
    ComplexBallFields(digits) elements
    
    Uses mpsolve as an external program, through subprocess
    and parse its result
    """
    CBF = ComplexBallField(digits)
    
    
    #On fait la substitution
    Ring2.<x> = PolynomialRing(ZZ,'x',sparse=True)
    P = Ring2(P_inf(y=x^d)) 
    
    # On prépare la commande mpsolve (Ga: goal is approximate, -o digits: digits of outpout, 
                                    #-So: only the roots with modulus >1; -Oc: compact output
                                    #-j8: tries to use several threads
    #pour cela on utilise un fichier temp pour stocker le polynôme
    pol_file = os.getcwd()+'/temp.pol'
    string_mpsolve ="""Monomial;
Sparse;
Real;
Integer;
Degree=%i;

"""%(P.degree())
    with open(pol_file,'w') as temp:
        temp.write(string_mpsolve)
        for k,v in P.dict().items():
            temp.write("%i, %i \n"%(k,v))
    args = shlex.split("mpsolve -Ga -o%i -j8 -So -Oc %s"%(digits,pol_file))
    #On la lance en récupérant la sortie
    to_parse = subprocess.Popen(args, stdout=subprocess.PIPE).stdout.read()
    roots_list = []
    #On parse les racines trouvées par MPsolve
    for coor in to_parse.decode().split('\n')[:-1]:
        coor = coor.strip('()').split(',')
        roots_list += [CBF(sage_eval(coor[0])+I*sage_eval(coor[1]))]
        
    # On supprime le fichier temporaire
    os.remove(pol_file)
    return roots_list

print("""Import fini
""")
